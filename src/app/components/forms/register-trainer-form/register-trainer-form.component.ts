import { Component, Output, EventEmitter } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { TrainerService } from '../../../services/trainer.service';

@Component({
  selector: 'app-register-trainer-form',
  templateUrl: './register-trainer-form.component.html',
  styleUrls: [ './register-trainer-form.component.css' ]
})
export class RegisterTrainerFormComponent {

  @Output() registered: EventEmitter<void> = new EventEmitter();

  public registerTrainerForm: FormGroup = new FormGroup({
    trainerName: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(28)
    ])
  });

  constructor(private trainerService: TrainerService) {
  }

  // Getters
  public get trainerName(): AbstractControl {
    return this.registerTrainerForm.get('trainerName');
  }

  // Event handlers
  public onRegisterClicked(): void {
    this.trainerService.register( this.trainerName.value.trim() );
    this.registered.emit();
  }

}
