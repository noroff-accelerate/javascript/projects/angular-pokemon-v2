import { Component, Input, OnInit } from '@angular/core';
import { AbilityService } from '../../../services/ability.service';

@Component( {
  selector: 'app-pokemon-abilities',
  templateUrl: './pokemon-abilities.component.html',
  styleUrls: [ './pokemon-abilities.component.css' ]
} )
export class PokemonAbilitiesComponent implements OnInit {

  @Input() abilityUrls: string[];
  public abilities: any[] = [];
  public loading = true;
  public error: string;

  constructor(private abilityService: AbilityService) {
  }

  // Lifecycles
  async ngOnInit(): Promise<any> {
    return await this.getAbilities();
  }

  // Methods
  public async getAbilities(): Promise<any> {
    this.abilities = [];
    this.loading = true;
    // const ids: string[] = this.pokemon.abilities.map( ({ability}) => ability.url );
    try {
      this.abilities = await this.abilityService.getAbilities( this.abilityUrls );
    } catch (e) {
      this.error = e.message;
    } finally {
      // Not loading abilities.
      this.loading = false;
    }
    return true;
  }

}
