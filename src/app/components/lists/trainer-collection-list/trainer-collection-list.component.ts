import { Component } from '@angular/core';
import { TrainerCollectionService } from 'src/app/services/trainer-collection.service';
import { Observable } from 'rxjs';
import { Pokemon } from '../../../models/pokemon.model';

@Component( {
  selector: 'app-trainer-collection-list',
  templateUrl: './trainer-collection-list.component.html',
  styleUrls: [ './trainer-collection-list.component.css' ]
} )
export class TrainerCollectionListComponent {

  constructor(private trainerCollectionService: TrainerCollectionService) {
  }

  // Getters
  get collectionList$(): Observable<Pokemon[]> {
    return this.trainerCollectionService.pokemonCollection;
  }

  // Event handlers
  public onRemoveClicked(id): void {
    if (confirm( 'Are you sure?' )) {
      this.trainerCollectionService.removeFromCollection( id );
    }
  }

}
