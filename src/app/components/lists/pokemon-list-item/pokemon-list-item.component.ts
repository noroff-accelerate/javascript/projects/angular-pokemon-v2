import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component( {
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: [ './pokemon-list-item.component.css' ]
} )
export class PokemonListItemComponent {

  @Input() pokemon: Pokemon;
  @Output() pokemonClicked: EventEmitter<number> = new EventEmitter();

  onPokemonClick(): void {
    this.pokemonClicked.emit( this.pokemon.id );
  }

}
