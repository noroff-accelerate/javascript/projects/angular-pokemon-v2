import { Component } from '@angular/core';
import { TrainerService } from '../../../services/trainer.service';
import { Observable } from 'rxjs';

@Component( {
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: [ './navbar.component.css' ]
} )
export class NavbarComponent {

  constructor(private trainerService: TrainerService) {
  }

  // Getters
  get trainerName(): Observable<string> {
    return this.trainerService.getTrainer();
  }

}
