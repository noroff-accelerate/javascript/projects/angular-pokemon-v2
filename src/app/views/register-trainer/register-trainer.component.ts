import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TrainerService } from '../../services/trainer.service';

@Component( {
  selector: 'app-register-trainer',
  templateUrl: './register-trainer.component.html',
  styleUrls: [ './register-trainer.component.css' ]
} )
export class RegisterTrainerComponent {

  constructor(private router: Router, private trainerService: TrainerService) {
    if (this.trainerService.hasTrainer()) {
      this.router.navigateByUrl( '/pokemon' );
    }
  }

  // Event handlers
  async onTrainerRegistered(): Promise<boolean> {
    return await this.router.navigateByUrl( '/pokemon' );
  }


}
