import { Component } from '@angular/core';
import { TrainerService } from '../../services/trainer.service';
import { Router } from '@angular/router';

@Component( {
  selector: 'app-trainer-profile',
  templateUrl: './trainer-profile.component.html',
  styleUrls: [ './trainer-profile.component.css' ]
} )
export class TrainerProfileComponent {

  constructor(private trainerService: TrainerService, private router: Router) {
  }

  // Event handlers
  async onLogoutClicked(): Promise<boolean> {
    if (confirm( 'Are you sure?' )) {
      this.trainerService.logout();
      return await this.router.navigateByUrl( '/register' );
    }
    return false;
  }
}
