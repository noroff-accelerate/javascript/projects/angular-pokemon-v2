import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonService } from '../../services/pokemon.service';
import { PokemonDetailService } from '../../services/pokemon-detail.service';
import { Subscription } from 'rxjs';
import { Pokemon } from '../../models/pokemon.model';
import { TrainerCollectionService } from '../../services/trainer-collection.service';
import { AbilityService } from '../../services/ability.service';

@Component( {
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: [ './pokemon-detail.component.css' ]
} )
export class PokemonDetailComponent implements OnInit {

  private readonly pokemonId: number = null;
  private pokemon$: Subscription;
  public pokemon: Pokemon = null;
  public abilities: any[] = [];

  constructor(private route: ActivatedRoute,
              private pokemonService: PokemonDetailService,
              private trainerCollectionService: TrainerCollectionService,
              private abilityService: AbilityService) {

    this.pokemonId = Number( this.route.snapshot.params.id );

    this.pokemon$ = this.pokemonService.pokemon$.subscribe( pokemon => {
      this.pokemon = pokemon;
    } );
  }

  // Getters
  get hasPokemon(): boolean {
    return this.trainerCollectionService.hasPokemonInCollection( this.pokemonId );
  }
  get isLoading(): boolean {
    return this.pokemonService.loading;
  }

  // Lifecycles
  ngOnInit(): void {
    this.pokemonService.getPokemon( this.pokemonId );
  }

  // Event handlers
  onCollectClicked(): void {
    this.trainerCollectionService.addToCollection( this.pokemon );
  }
  onRemoveClicked(): void {
    this.trainerCollectionService.removeFromCollection( this.pokemonId );
  }

  // Methods
  getAbilityUrls(): string[] {
    return this.pokemon.abilities.map( ({ability}) => ability.url );
  }

}
