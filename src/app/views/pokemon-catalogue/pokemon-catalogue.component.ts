import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { PokemonService } from '../../services/pokemon.service';
import { Subscription } from 'rxjs';
import { Pokemon } from '../../models/pokemon.model';
import { Router } from '@angular/router';

@Component( {
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: [ './pokemon-catalogue.component.css' ]
} )
export class PokemonCatalogueComponent implements OnInit, OnDestroy {

  private pokemon$: Subscription;
  public pokemon: Pokemon[] = [];
  private searchText: string = '';

  constructor(private pokemonService: PokemonService, private router: Router) {
    this.pokemon$ = this.pokemonService.pokemon$.subscribe( (pokemon: Pokemon[]) => {
      this.pokemon = pokemon;
    } );
  }

  // Getters
  get totalPages(): number {
    return this.pokemonService.pages;
  }
  get currentPage(): number {
    return this.pokemonService.currentPage;
  }
  get isLoading(): boolean {
    return this.pokemonService.loading;
  }

  // Methods
  getPokemon(): Pokemon[] {
    return this.pokemon.filter( poke => {
      return poke.name.toLowerCase().includes( this.searchText.toLowerCase().trim() );
    } );
  }

  // Event handlers
  onPrevClick(): void {
    this.pokemonService.prevPage();
  }
  onNextClick(): void {
    this.pokemonService.nextPage();
  }
  onPokemonClick(id): void {
    this.router.navigateByUrl( `/pokemon/${ id }` );
  }
  onSearchChange(searchText: string): void {
    this.searchText = searchText;
  }

  // Lifecycles
  ngOnInit(): void {
    this.pokemonService.getPokemon();
  }
  ngOnDestroy(): void {
    this.pokemon$.unsubscribe();
  }
}
