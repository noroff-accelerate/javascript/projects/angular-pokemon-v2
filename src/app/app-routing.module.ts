import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterTrainerComponent } from './views/register-trainer/register-trainer.component';
import { PokemonCatalogueComponent } from './views/pokemon-catalogue/pokemon-catalogue.component';
import { TrainerProfileComponent } from './views/trainer-profile/trainer-profile.component';
import { AuthGuard } from './guards/auth.guard';
import { PokemonDetailComponent } from './views/pokemon-detail/pokemon-detail.component';
import { NotFoundComponent } from './views/not-found/not-found.component';

const routes: Routes = [
  {
    path: 'register',
    component: RegisterTrainerComponent
  },
  {
    path: 'pokemon',
    component: PokemonCatalogueComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'pokemon/:id',
    component: PokemonDetailComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'trainer',
    component: TrainerProfileComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/register'
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule( {
  imports: [ RouterModule.forRoot( routes ) ],
  exports: [ RouterModule ]
} )
export class AppRoutingModule {
}
