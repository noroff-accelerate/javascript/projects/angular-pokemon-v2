import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { BehaviorSubject } from 'rxjs';

// Constants
const STORAGE_KEY = 'trainer-collection';

@Injectable( {
  providedIn: 'root'
} )
export class TrainerCollectionService {

  // Observables
  public pokemonCollection: BehaviorSubject<Pokemon[]> = new BehaviorSubject( [] );

  constructor() {
    this.init();
  }

  // Private methods
  private init(): void {
    const savedCollection = localStorage.getItem( STORAGE_KEY );
    if (savedCollection) {
      this.pokemonCollection.next( JSON.parse( savedCollection ) );
    }
  }

  private save(): void {
    localStorage.setItem( STORAGE_KEY, JSON.stringify( this.pokemonCollection.value ) );
  }

  // Methods
  public hasPokemonInCollection(pokemonId: number): boolean {
    return Boolean( this.pokemonCollection.value.find( (pokemon: Pokemon) => {
      return pokemon.id === pokemonId;
    } ) );
  }

  public addToCollection(newPokemon: Pokemon): boolean {
    const currentCollection = [ ...this.pokemonCollection.value ];

    if (this.hasPokemonInCollection( newPokemon.id )) {
      return false;
    }

    currentCollection.push( newPokemon );
    this.pokemonCollection.next( currentCollection );
    this.save();
    return true;
  }

  public removeFromCollection(removeId: number): void {
    const currentCollection = [ ...this.pokemonCollection.value ];
    const filteredCollection = currentCollection.filter( (p: Pokemon) => {
      return p.id !== removeId;
    } );

    this.pokemonCollection.next( filteredCollection );
    this.save();
  }
}
