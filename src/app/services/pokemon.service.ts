import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment as ENV } from '../../environments/environment';
import { BehaviorSubject, from, Observable, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable( {
  providedIn: 'root'
} )
export class PokemonService {

  // Observables
  public readonly pokemon$: BehaviorSubject<Pokemon[]> = new BehaviorSubject( [] );
  // Public properties
  public offset = 0;
  public limit = 2000;
  public pages = 0;
  public currentPage = 1;
  public totalRecords = 0;
  public loading: boolean = false;

  constructor(private http: HttpClient) {
  }

  // Property modifiers
  nextPage(): void {
    this.offset += this.limit;
    this.currentPage++;
    this.getPokemon();
  }
  prevPage(): void {
    this.offset -= this.limit;
    this.currentPage--;
    this.getPokemon();
  }

  // Private methods - Should maybe be a util file?
  private getIdAndImage(url: string): any {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return {id, url: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }

  // Http requests.
  getPokemon(): void {
    this.loading = true;


    this.http.get<Pokemon[]>( `${ ENV.apiBaseUrl }/pokemon?limit=${ this.limit }&offset=${ this.offset }` )
      .pipe(
        tap( (response: any) => {
          this.totalRecords = response.count;
          this.pages = Math.ceil( this.totalRecords / this.limit );
        } ),
        map( (response: any) => response.results.map( p => {
            return {
              ...p,
              ...this.getIdAndImage( p.url )
            };
          }
        ) )
      ).toPromise().then( (p: Pokemon[]) => {
      this.pokemon$.next( p );
      this.loading = false;
    } );
  }
}
