import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable( {
  providedIn: 'root'
} )
export class PokemonDetailService {

  // Observables
  pokemon$: BehaviorSubject<Pokemon> = new BehaviorSubject( null );
  // Public properties
  public loading: boolean = false;

  constructor(private http: HttpClient) {
  }

  // Http Requests
  getPokemon(id): void {

    this.loading = true;
    this.pokemon$.next( null );

    this.http.get( `${ environment.apiBaseUrl }/pokemon/${ id }` ).subscribe( (pokemon: Pokemon) => {
      this.loading = false;
      this.pokemon$.next( pokemon );
    } );
  }
}
