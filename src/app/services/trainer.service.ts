import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

// Constants
const STORAGE_KEY = 'poketrainer';

@Injectable( {
  providedIn: 'root'
} )
export class TrainerService {

  // Observables
  private trainer$: BehaviorSubject<string> = new BehaviorSubject( '' );

  constructor() {
    const savedTrainer = localStorage.getItem( STORAGE_KEY );
    if (savedTrainer) {
      this.trainer$.next( savedTrainer );
    }
  }

  // Methods
  hasTrainer(): boolean {
    return Boolean( this.trainer$.value );
  }

  getTrainer(): Observable<string> {
    return this.trainer$.asObservable();
  }

  register(name: string): void {
    this.trainer$.next( name );
    localStorage.setItem( STORAGE_KEY, name );
  }

  logout(): void {
    localStorage.removeItem( STORAGE_KEY );
    this.trainer$.next( '' );
  }
}
