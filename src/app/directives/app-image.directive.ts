import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';

@Directive( {
  selector: '[appImage]'
} )
export class AppImageDirective implements AfterViewInit {

  @Input() src;

  constructor(private imageRef: ElementRef) {}

  ngAfterViewInit(): void {
    const image = new Image();
    image.onload = () => {
      this.setImage( this.src );
    };
    image.onerror = () => {
      this.setImage( 'assets/pokeball.png' );
    };
    image.src = this.src;
  }

  private setImage(src: string): void {
    this.imageRef.nativeElement.setAttribute( 'src', src );
  }

}
