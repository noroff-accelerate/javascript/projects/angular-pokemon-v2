export interface PokemonSprite {
  front_default: string;
  back_default: string;
  back_shiny: string;
  front_shiny: string;
}
