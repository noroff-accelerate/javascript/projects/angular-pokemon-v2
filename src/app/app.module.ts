import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterTrainerComponent } from './views/register-trainer/register-trainer.component';
import { TrainerProfileComponent } from './views/trainer-profile/trainer-profile.component';
import { PokemonCatalogueComponent } from './views/pokemon-catalogue/pokemon-catalogue.component';
import { RegisterTrainerFormComponent } from './components/forms/register-trainer-form/register-trainer-form.component';
import { PokemonListItemComponent } from './components/lists/pokemon-list-item/pokemon-list-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { SearchBarComponent } from './components/forms/search-bar/search-bar.component';
import { PokemonDetailComponent } from './views/pokemon-detail/pokemon-detail.component';
import { TrainerCollectionListComponent } from './components/lists/trainer-collection-list/trainer-collection-list.component';
import { AppImageDirective } from './directives/app-image.directive';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { PokemonAbilitiesComponent } from './components/lists/pokemon-abilities/pokemon-abilities.component';
import { NotFoundComponent } from './views/not-found/not-found.component';

@NgModule( {
  declarations: [
    AppComponent,
    RegisterTrainerComponent,
    TrainerProfileComponent,
    PokemonCatalogueComponent,
    RegisterTrainerFormComponent,
    PokemonListItemComponent,
    NavbarComponent,
    SearchBarComponent,
    PokemonDetailComponent,
    TrainerCollectionListComponent,
    AppImageDirective,
    PokemonAbilitiesComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    LazyLoadImageModule
  ],
  providers: [],
  bootstrap: [ AppComponent ]
} )
export class AppModule {
}
